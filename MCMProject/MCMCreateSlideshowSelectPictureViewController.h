//
//  MCMCreateSlideshowSelectPictureViewController.h
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCMCreateSlideshowSelectPictureViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *pictures;
}
@end
