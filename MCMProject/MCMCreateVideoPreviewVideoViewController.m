//
//  MCMCreateVideoPreviewVideoViewController.m
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import "MCMCreateVideoPreviewVideoViewController.h"

#import "MCMFileManagerViewController.h"

@interface MCMCreateVideoPreviewVideoViewController ()

@end

@implementation MCMCreateVideoPreviewVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Video Preview";
    //Button "Done"
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = doneBtn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)done:(id)sender
{
    MCMFileManagerViewController *fileManagerVC = [[MCMFileManagerViewController alloc] init];
    [self.navigationController pushViewController:fileManagerVC animated:YES];
}
@end
