//
//  MCMCreateVideoSelectMusicViewController.m
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import "MCMCreateVideoSelectMusicViewController.h"

#import "MCMCreateVideoRecordVideoViewController.h"

@interface MCMCreateVideoSelectMusicViewController ()

@end

@implementation MCMCreateVideoSelectMusicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Select Music";
    songs = [[NSMutableArray alloc] initWithObjects:@"file_name1.mp3", @"file_name2.mp3", @"file_name3.mp3",@"file_name1.mp3", @"file_name2.mp3", @"file_name3.mp3",@"file_name1.mp3", @"file_name2.mp3", @"file_name3.mp3",@"file_name1.mp3", @"file_name2.mp3", @"file_name3.mp3", nil];
    //Button "Done"
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = doneBtn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [songs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [songs objectAtIndex:[indexPath row]];
    
    return cell;
}

-(IBAction)done:(id)sender
{
    MCMCreateVideoRecordVideoViewController *recordVideoVC = [[MCMCreateVideoRecordVideoViewController alloc] init];
    [self.navigationController pushViewController:recordVideoVC animated:YES];
}
@end
