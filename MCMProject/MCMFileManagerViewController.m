//
//  MCMFileManagerViewController.m
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import "MCMFileManagerViewController.h"

@interface MCMFileManagerViewController ()

@end

@implementation MCMFileManagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"File Manager";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
