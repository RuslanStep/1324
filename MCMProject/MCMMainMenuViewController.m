//
//  MCMMainMenuViewController.m
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import "MCMMainMenuViewController.h"

#import "MCMRecordVoiceViewController.h"
#import "MCMCreateVideoSelectMusicViewController.h"
#import "MCMCreateSlideshowSelectPictureViewController.h"
#import "MCMFileManagerViewController.h"

@interface MCMMainMenuViewController ()

@end

@implementation MCMMainMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Main Menu";
	//Button "Record Voice"
    UIButton *recordVoiceBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [recordVoiceBtn addTarget:self
                       action:@selector(recordVoice:)
             forControlEvents:UIControlEventTouchDown];
    [recordVoiceBtn setTitle:@"Record Voice" forState:UIControlStateNormal];
    recordVoiceBtn.frame = CGRectMake(80.0, 110.0, 160.0, 40.0);
    [self.view addSubview:recordVoiceBtn];
    //Button "Create Video"
    UIButton *createVideoBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createVideoBtn addTarget:self
                       action:@selector(createVideo:)
             forControlEvents:UIControlEventTouchDown];
    [createVideoBtn setTitle:@"Create Video" forState:UIControlStateNormal];
    createVideoBtn.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    [self.view addSubview:createVideoBtn];
    //Button "Create Slideshow"
    UIButton *createSlideshowBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createSlideshowBtn addTarget:self
                       action:@selector(createSlideshow:)
             forControlEvents:UIControlEventTouchDown];
    [createSlideshowBtn setTitle:@"Create Slideshow" forState:UIControlStateNormal];
    createSlideshowBtn.frame = CGRectMake(80.0, 310.0, 160.0, 40.0);
    [self.view addSubview:createSlideshowBtn];
    //Button "File Manager"
    UIButton *fileManagerBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [fileManagerBtn addTarget:self
                       action:@selector(showFileManager:)
             forControlEvents:UIControlEventTouchDown];
    [fileManagerBtn setTitle:@"File Manager" forState:UIControlStateNormal];
    fileManagerBtn.frame = CGRectMake(80.0, 410.0, 160.0, 40.0);
    [self.view addSubview:fileManagerBtn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)recordVoice:(id)sender
{
    MCMRecordVoiceViewController *recordVoiceVC = [[MCMRecordVoiceViewController alloc] init];
    [self.navigationController pushViewController:recordVoiceVC animated:YES];
}

-(IBAction)createVideo:(id)sender
{
    MCMCreateVideoSelectMusicViewController *createVideoSMVC = [[MCMCreateVideoSelectMusicViewController alloc] init];
    [self.navigationController pushViewController:createVideoSMVC animated:YES];
}

-(IBAction)createSlideshow:(id)sender
{
    MCMCreateSlideshowSelectPictureViewController *createSlideshowSPVC = [[MCMCreateSlideshowSelectPictureViewController alloc] init];
    [self.navigationController pushViewController:createSlideshowSPVC animated:YES];
}

-(IBAction)showFileManager:(id)sender
{
    MCMFileManagerViewController *fileManagerVC = [[MCMFileManagerViewController alloc] init];
    [self.navigationController pushViewController:fileManagerVC animated:YES];
}
@end
