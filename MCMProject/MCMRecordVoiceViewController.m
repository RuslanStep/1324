//
//  MCMRecordVoiceViewController.m
//  MCMProject
//
//  Created by Ruslan Stepanov on 10/20/13.
//  Copyright (c) 2013 Ruslan Stepanov. All rights reserved.
//

#import "MCMRecordVoiceViewController.h"

#import "MCMFileManagerViewController.h"

@interface MCMRecordVoiceViewController ()

@end

@implementation MCMRecordVoiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Record Voice";
    //Button "Record"
    UIButton *recordBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [recordBtn addTarget:self
                  action:@selector(record:)
        forControlEvents:UIControlEventTouchDown];
    [recordBtn setTitle:@"Rec" forState:UIControlStateNormal];
    recordBtn.frame = CGRectMake(10.0, 80.0, 80.0, 40.0);
    [self.view addSubview:recordBtn];
    //Button "Play"
    UIButton *playBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [playBtn addTarget:self
                action:@selector(play:)
      forControlEvents:UIControlEventTouchDown];
    [playBtn setTitle:@"Play" forState:UIControlStateNormal];
    playBtn.frame = CGRectMake(110.0, 80.0, 80.0, 40.0);
    [self.view addSubview:playBtn];
    //Slider voice/music balance
    UISlider *vmBalanceSldr = [[UISlider alloc] init];
    [vmBalanceSldr addTarget:self
                      action:@selector(changeVMBalance:)
            forControlEvents:UIControlEventValueChanged];
    vmBalanceSldr.frame = CGRectMake(210.0, 80.0, 80.0, 40.0);
    vmBalanceSldr.minimumValue = 0.0;
    vmBalanceSldr.maximumValue = 100.0;
    vmBalanceSldr.continuous = YES;
    vmBalanceSldr.value = 50.0;
    [self.view addSubview:vmBalanceSldr];
    //Slider music
    UISlider *musicSldr = [[UISlider alloc] init];
    [musicSldr addTarget:self
                      action:@selector(changeMusic:)
            forControlEvents:UIControlEventValueChanged];
    musicSldr.frame = CGRectMake(10.0, 130.0, 300.0, 40.0);
    musicSldr.minimumValue = 0.0;
    musicSldr.maximumValue = 100.0;
    musicSldr.continuous = YES;
    musicSldr.value = 0.0;
    [self.view addSubview:musicSldr];
    //Button "Select File"
    UIButton *selectFileBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [selectFileBtn addTarget:self
                      action:@selector(selectFile:)
            forControlEvents:UIControlEventTouchDown];
    [selectFileBtn setTitle:@"Select File" forState:UIControlStateNormal];
    selectFileBtn.frame = CGRectMake(0.0, 180.0, 160.0, 40.0);
    [self.view addSubview:selectFileBtn];
    //Label "File Name"
    UILabel *fileNameLbl = [[UILabel alloc] init];
    [fileNameLbl setText:@"file_name.mp3"];
    fileNameLbl.frame = CGRectMake(160.0, 180.0, 160.0, 40.0);
    [self.view addSubview:fileNameLbl];
    //TabelView "Effects"
    UITableView *effectsTV = [[UITableView alloc] init];
    effectsTV.frame = CGRectMake(10.0, 230.0, 300.0, 100.0);
    effectsTV.dataSource = self;
    effectsTV.delegate = self;
    [self.view addSubview:effectsTV];
    //TabelView "Voice preset"
    UITableView *voicePresetTV = [[UITableView alloc] init];
    voicePresetTV.frame = CGRectMake(10.0, 360.0, 300.0, 100.0);
    voicePresetTV.dataSource = self;
    voicePresetTV.delegate = self;
    [self.view addSubview:voicePresetTV];
    //Button "Done"
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = doneBtn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)record:(id)sender
{
    
}

-(IBAction)play:(id)sender
{
    
}

-(IBAction)changeVMBalance:(id)sender
{
    
}

-(IBAction)changeMusic:(id)sender
{
    
}

-(IBAction)selectFile:(id)sender
{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *CellIdentifier = @"CellExample";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = @"Example Text";
    
    return cell;
}

-(IBAction)done:(id)sender
{
    MCMFileManagerViewController *fileManagerVC = [[MCMFileManagerViewController alloc] init];
    [self.navigationController pushViewController:fileManagerVC animated:YES];
}
@end
